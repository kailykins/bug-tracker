CREATE TABLE
    IF NOT EXISTS test (ID SERIAL PRIMARY KEY NOT NULL, Name VARCHAR(128));

INSERT INTO
    test (Name)
VALUES
    ('foo'),
    ('bar');

CREATE TABLE
    IF NOT EXISTS example (
        ID UUID PRIMARY KEY DEFAULT gen_random_uuid (),
        Title VARCHAR(128),
        Description VARCHAR(2048),
        Priority INT DEFAULT 5,
        Owner VARCHAR(32) DEFAULT 'undefined'
    );

INSERT INTO
    example (Title, Description, Priority)
VALUES
    (
        'product description link sends user to 404 error',
        'user cannot see detailed description about product when clicking on link; instead user is sent to 404 not found error page',
        4
    ),
    (
        'cannot remove address from address book',
        'user cannot remove previous address from address book. When clicking remove nothing happens',
        3
    );