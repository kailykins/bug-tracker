#!/bin/bash
apt update
apt --yes upgrade
locale-gen en_GB.UTF-8
update-locale LANG=en_GB.utf8 LANGUAGE
echo "Europe/London" > /etc/timezone
