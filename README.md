# BugTracker Test

This is a repo to prove and try technologies that i have not had the chance to since starting to work as a Data Engineer in very complex working systems in the Data Engineering space - Big Data at Infinity Works for NHS as a client.
This is a very minimal setup and just for personal experience.

![Screenshot sample](./bugTracker.png "Rudimentary Table View of the bugs")

## Things that i would like to add on

### Add authentication and authorization layer

This would be needed to authenticate the user using the api and verify their actions, that they are authorised to access different parts of the system.

### Better UI/UX and features/capabilities

So that this app could be actually useful for its intended purpose as a bug tracker:

- Navigation from home page to different pages including the bugs view
- Login system
- Project selection on bugs view
- Add tickets
- Select and view more details for each bug
- Assign tickets to different people/users
- Show a package of resources like screenshots and steps to reproduce the 'bug' tied to the ticked uuid
- Add ticket flow/stages - i.e. from 'Triage' through 'Current sprint fix' to 'Solved - Reported back to user'
- Add comments/notes area to report on progress
- Change ticket priority
- Add ability to archive/close tickets

Also on the management side:

- Ability to add users
- Modify users permissions
- Add different management functions for administrators i.e. locking certain projects, tickets or making the website unavailable...
- Display maintenance messages for others

### Create Terraform orchestration layer

As part of becoming production ready, this would be needed so it's easily deployed and maintained in a cloud environment. This could be done in serverless too or AWS' CDK.

### Testing

For CI-CD is important to have a comprehensive set of unit and integration tests to guarantee the reliability of the new features and bug-fixes deployed. For this would be needed:

- Frontend interaction tests
- Backend endpoints tests
- Integration of frontend with backend tests
- Accessibility tests for any existing and new components on frontend

### CI-CD

I would like to have created a jenkins job or similar (used Github actions in the past) to create the docker images and upload them to a registry, so that it would be deployed with regular updates and bug fixes to a production environment.

### Scalability

This project is at most a proof of concept. The ammount of connections maintained by the backend to the database should be at least 10 or so to maximise performance between the two.
Depending on the amount of concurrent users expected there should be more than one backend server behind a load balancer. If there would be many multiple clients it would be difficult to have one database server managing many 10s of connections then it would be needed to scale up horizontally to a managed shared database system to be able to handle all these. PostgreSQL might not be the best for this though.

### Upgrade the .devcontainer

This has the capability of adding an emulation layer for machines on ARM architecture if needed. There are other adaptations needed for this though like

## To test out

There are three working endpoints from the frontend for now:

- `/` - root directory
- `/dbtest` - end to end test connection. Database (postgres), backend(express.js) and frontend(axios and react)
- `/<anything here>` - Any other endpoint sends you to the error page

You can bring the project up by running `docker-compose up -d` on the root of the repo.

You can bring all the project containers down by running `docker-compose down`.

## To develop on

There is a '.devcontainer' config for a containerised environment. This is very useful from a VSCode IDE standpoint but can be used with some adaptations from IDE's like JetBrains.

### Database start-up

Run `docker-compose up -d db` to start the postgres container.
The database has resilient storage as a docker volume.
You can reset it by listing all the docker volumes present `docker volume ls` and then removing the 'db' one, i.e. `docker volume rm workspace_db`. When you bring the container up again it will reinitialize the volume and database.

You can login by running `docker exec -it <docker container id> /bin/bash` by replacing the 'docker container id' with the short hash code on the first field of the table returned by running `docker ps` to list all running docker containers. For example 'f054e149b431'. To login by using 'psql' you need to specify the 'postgres' user: `psql -U postgres`.

### Backend start-up

Change directory to `./backend`. Run `node main` to start the backend.

### Frontend start-up

Change directory to `./frontend`. Run `npm start` to start the frontend.
