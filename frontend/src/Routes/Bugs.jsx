import { Card, Table } from "react-bootstrap";
import { useState, useEffect } from "react";
import { getEndpoint } from "../api_client";

const table_columns = ["ID", "Title", "Description", "Priority", "Owner"];
const table_render = (table_rows) => {
  let rendered_header = (
    <tr>
      {table_columns.map((column) => (
        <th>{column}</th>
      ))}
    </tr>
  );
  let rendered_body = [];
  table_rows.forEach((row) => {
    const row_keys = Object.keys(row);
    let rendered_row = (
      <tr>
        {row_keys.map((key) => (
          <td>{row[key]}</td>
        ))}
      </tr>
    );
    rendered_body.push(rendered_row);
  });
  return (
    <Table striped bordered hover>
      <thead>{rendered_header}</thead>
      <tbody>{rendered_body}</tbody>
    </Table>
  );
};

export default function Bugs() {
  const [isLoading, setIsLoading] = useState(true);
  const [result, setResult] = useState("");
  useEffect(() => {
    getEndpoint("project/example").then((response) => {
      setResult(table_render(response));
      setIsLoading(false);
    });
  }, []);
  return (
    <body>
      <Card>
        <Card.Body>
          <Card.Title className="mt-3">Bugs Tickets</Card.Title>
          {isLoading ? "Loading" : ""}
          {result}
        </Card.Body>
      </Card>
    </body>
  );
}
