import { Card } from "react-bootstrap";
import { useState, useEffect } from "react";
import { getEndpoint } from "../api_client";

export default function DBTest() {
  const [isLoading, setIsLoading] = useState(true);
  const [result, setResult] = useState("");
  useEffect(() => {
    getEndpoint("pgtest").then((response) => {
      console.log('"' + response.name + '"');
      setResult(
        response.name === "bar" ? "Working" : "Error: " + response.name
      );
      setIsLoading(false);
    });
  }, []);
  return (
    <body>
      <Card>
        <Card.Body>
          <Card.Title className="mt-3">Database Test</Card.Title>
          {isLoading ? "Loading" : ""}
          {result}
        </Card.Body>
      </Card>
    </body>
  );
}
