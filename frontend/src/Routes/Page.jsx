// Generic Page functions for all Routes

export default function Page(props) {
  if (props.title) document.title = props.title;
  const PageToRender = props.element;
  return <PageToRender props={props} />;
}
