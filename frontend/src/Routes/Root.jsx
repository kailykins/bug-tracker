import { Card } from "react-bootstrap";

export default function Root() {
  return (
    <body>
      <Card>
        <Card.Body>
          <Card.Img src="https://picsum.photos/200/100" />
          <Card.Title className="mt-3">This is title</Card.Title>
          This is body
        </Card.Body>
      </Card>
    </body>
  );
}
