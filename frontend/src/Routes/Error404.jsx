import { Button, Alert, Card } from "react-bootstrap";

export default function Error404(props) {
  return (
    <>
      <header className="App-header">
        <strong>404 - Not Found</strong>
      </header>

      <body>
        <Card>
          <Alert className="alert alert-danger" role="alert">
            Not Found
          </Alert>
          <Button href="/">Back Home</Button>
        </Card>
      </body>
    </>
  );
}
