import axios from "axios";

const apiEndPoint = "localhost";
const port = 8001;

function getEndpoint(endpoint) {
  return axios(`http://${apiEndPoint}:${port}/${endpoint}`)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      console.error("Error on querying API: " + e);
    });
}

export { getEndpoint };
