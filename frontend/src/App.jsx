import "./App.css";
import Router from "./Router";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Bug Tracker
      </header>
        <Router />
      <footer>© kaily newman 2022</footer>
    </div>
  );
}

export default App;
