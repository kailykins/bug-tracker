import { BrowserRouter, Route, Routes } from "react-router-dom";
import Page from "./Routes/Page";
import Bugs from "./Routes/Bugs";
import DBTest from "./Routes/DBTest";
import Root from "./Routes/Root";
import Error404 from "./Routes/Error404";

export default function Router() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          exact
          path="/bugs"
          element={<Page element={Bugs} title={"Bug Tracker - Bugs"} />}
        />
        <Route
          exact
          path="/dbtest"
          element={
            <Page element={DBTest} title={"Bug Tracker - Database Test"} />
          }
        />
        <Route path="/" element={<Page element={Root} />} />
        <Route
          path="/*"
          element={
            <Page element={Error404} title={"Bug Tracker - Page Not Found"} />
          }
        />
      </Routes>
    </BrowserRouter>
  );
}
