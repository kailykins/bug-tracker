const { Pool } = require("pg");
const pg_pool = new Pool({
  host: "host.docker.internal",
  port: 5432,
  user: "postgres",
  password: "example",
});

pg_pool.on("connect", () => {
  console.log("connected to DB");
});

pg_pool.on("error", () => {
  console.error("connection error to DB", error);
});

module.exports = { pg_pool };
