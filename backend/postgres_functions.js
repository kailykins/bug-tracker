const { pg_pool } = require("./postgres_client.js");
const pg_query_formater = require("pg-format");

const db_test_query = "SELECT Name FROM Test WHERE ID=2;";
const db_test_promise = function () {
  return pg_pool
    .query(db_test_query)
    .then((pg_res) => pg_res.rows[0])
    .catch((error) => {
      res.send("DB error");
      console.error(error);
    });
};

const projects_query =
  "SELECT * FROM information_schema.tables \
WHERE table_type = 'BASE TABLE' \
AND table_schema = 'public';";
const existing_projects_list_promise = function () {
  return pg_pool
    .query(projects_query)
    .then((pg_res) =>
      pg_res.rows
        .map((table) => table.table_name)
        .filter((table_name) => table_name !== "test")
    );
};

const project_bugs_query = "SELECT * FROM %I;";
const get_bug_list_promise = function (project_name) {
  return pg_pool
    .query(pg_query_formater(project_bugs_query, project_name))
    .then((db_res) => db_res.rows);
};

module.exports = {
  db_test_promise,
  existing_projects_list_promise,
  get_bug_list_promise,
};
