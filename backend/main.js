const express = require("express");
const cors = require("cors");
const { pg_client } = require("./postgres_client");
const {
  db_test_promise,
  existing_projects_list_promise,
  get_bug_list_promise,
} = require("./postgres_functions");

const app = express();
const port = 8001;

app.use(cors());

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/project/:project_name", (req, res) => {
  const { project_name } = req.params;
  existing_projects_list_promise().then((existing_projects) => {
    if (existing_projects.includes(project_name)) {
      get_bug_list_promise(project_name).then((project_bugs_table) => {
        res.json(project_bugs_table);
      });
    } else {
      res.status(404).json({
        error: "Not found or you don't have permission to access this project",
      });
      console.error("Failed attempt to access project: " + project_name);
    }
  });
});

app.get("/pgtest", (req, res) => {
  db_test_promise().then((db_response) => res.json(db_response));
});

app.listen(port, () => {
  console.log(`Backend listening on port ${port}`);
});
